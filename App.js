import React from 'react';
import {
  AppRegistry,
  Text,
  View,
  Button
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import CameraUsage from './Camera.js';


class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Welcome',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View>
        <Text>Welcome to the better version of splitwise!</Text>
        <Button
          onPress={() => navigate('Camera', { page: 'Camera' })}
          title="Get started"
        />
      </View>
    );
  }
}

class CameraScreen extends React.Component {
  // Nav options can be defined as a function of the screen's props:
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.page}`,
  });
  render() {
    // The screen's current route is passed in to `props.navigation.state`:
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    return (
      <View>
        <Text>This is the {params.page} page</Text>
        <Button
          onPress={() => navigate('List', { page: 'List of items' })}
          title="Capture"
        />
      </View>
    );
  }
}

class ListScreen extends React.Component {
  // Nav options can be defined as a function of the screen's props:
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.page}`,
  });
  render() {
    // The screen's current route is passed in to `props.navigation.state`:
    const { params } = this.props.navigation.state;
    return (
      <View>
        <Text>This is the {params.page} page</Text>
        
      </View>
    );
  }
}

const Navigation = StackNavigator({
  Home: { screen: HomeScreen },
  Camera: { screen: CameraUsage },
  List: { screen: ListScreen },
});

AppRegistry.registerComponent('Navigation', () => Navigation);