'use strict';
import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  Image,
  StatusBar
} from 'react-native';
import Camera from 'react-native-camera';

class CameraUsage extends Component {

  render() {
    return (
      <View style={styles.container}> 
        <Camera ref={
            (cam) => { this.camera = cam; }
          } 
          style={styles.preview} 
          aspect={Camera.constants.Aspect.fill}> 


          <Image style={{width: 55, height: 55}}
          onPress={this.takePicture.bind(this)} 
          source={require('./images/circle-xxl.png')} />
        </Camera> 
      </View>
    );
  }

  takePicture() {
    const options = {};
    //options.location = ...
    this.camera.capture({metadata: options})
      .then((data) => console.log(data))
      .catch(err => console.error(err));
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  }
});

module.exports = CameraUsage;